#include "bmp.h"
#include "image_rotate.h"

int main( int argc, char** argv ) {
	if(argc != 3){
		printf("Wrong argument count");
		exit(1);
	}
	struct image* im = bmp_read(argv[1]);
	struct image* new_im = rotate(*im);
	image_free(im);
	bmp_write(argv[2], new_im);
	image_free(new_im);
	fprintf(stdout, "\nSUCCESS");	
	return 0;
}
