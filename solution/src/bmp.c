#include "bmp.h"

#pragma pack(push, 2)

char* read_status_msg [ 6 ] = {
	"Everything is OK", 
	"Invalid signature", 
	"Invalid bits", 
	"Invalid header", 
	"Invalid reserved", 
	"File does not exist"
};

struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)

static bool read_header( FILE* f, struct bmp_header* header ) {
    return (fread( header, 1, sizeof( struct bmp_header ), f ) == sizeof( struct bmp_header ));
}

static bool read_pixel(FILE* f, struct bmp_header* header, struct pixel* pixel){
	fseek(f, header->bOffBits, SEEK_SET);
	uint32_t const width = header->biWidth;
	uint32_t const height = header->biHeight;
	uint16_t const count = header->biBitCount / 8;
	uint32_t const padding = (width*sizeof(struct pixel) % 4) == 0 ? 0 : 4 - (width*sizeof(struct pixel) % 4);
	for(int i = 0; i < height; i++){
		size_t result =  fread(&(pixel[i*width]), 1, width*count, f);
		if((uint32_t)result != (width*count)){
			if(feof(f)) printf("Premature end of file.");
			else perror("ERROR: ");
			return false;
		}
		if(fseek(f, padding, SEEK_CUR) != 0){
			perror("ERROR: ");
		};
	}
	return true;
}

enum read_status validate_header(struct bmp_header header){
	if (header.bfType != 0x4D42) return READ_INVALID_SIGNATURE;
	if (header.bfReserved != 0) return READ_INVALID_RESERVED;
	return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ){
	struct bmp_header header = { 0 }; 
	if(!read_header(in, &header))return READ_INVALID_HEADER;
	enum read_status header_status = validate_header(header);
	if(header_status != READ_OK){
		return header_status;
	}
	image_initialize(header.biWidth, header.biHeight, img);
	if(!read_pixel(in, &header, img->data)){
		image_free(img);
		return READ_INVALID_BITS;
	}
	
	return READ_OK;
}

static bool write_header( FILE* f, struct bmp_header* header ) {
    return (fwrite( header, 1, sizeof( struct bmp_header ), f ) == sizeof( struct bmp_header ));
}

static bool write_pixels(FILE* f, struct bmp_header* header, struct pixel* pixel){
	uint32_t const width = header->biWidth;
	uint32_t const height = header->biHeight;
	uint16_t const count = header->biBitCount / 8;
	uint32_t const padding = (width*count % 4) == 0 ? 0 : 4 - (width*count % 4);
	for(int i = 0; i < height; i++){
		size_t result =  fwrite(&(pixel[i*width]), 1, width*count, f);
		if((uint32_t)result != (width*count)){
			if(ferror(f)) perror("ERROR: ");
			return false;
		}
		if(fseek(f, padding, 1) != 0){
			perror("ERROR: ");
		};
	}
	return true;
}

struct bmp_header header_initialize(uint32_t width, uint32_t height){
	struct bmp_header header = { 0 };
	uint32_t bmp_header_size = sizeof(struct bmp_header);
	uint32_t padding = (width*sizeof(struct pixel) % 4) == 0 ? 0 : 4 - (width*sizeof(struct pixel) % 4);
	uint32_t pixels_size = (width*sizeof(struct pixel) + padding)*height;
	uint32_t file_size = bmp_header_size + pixels_size;

	header.bfType = 0x4D42;
	header.bfileSize = file_size;
	header.bfReserved = 0;
	header.bOffBits = bmp_header_size;
	header.biSize = 40;
	header.biWidth = width;
	header.biHeight = height;
	header.biPlanes = 1;
	header.biBitCount = 24;
	header.biCompression = 0;
	header.biSizeImage = pixels_size;
	header.biXPelsPerMeter = 0;
	header.biYPelsPerMeter = 0;
	header.biClrUsed = 0;
	header.biClrImportant = 0;
	return header;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
	
	uint32_t width = img->width;
	uint32_t height = img->height;

	struct bmp_header header = header_initialize(width, height);

	if(!write_header(out, &header) || !write_pixels(out, &header, img->data)){
		return WRITE_ERROR;
	}
	return WRITE_OK;
}

static void print_error(enum read_status answer){
	fprintf(stderr, "%s", read_status_msg[answer]);
	fprintf(stdout, "\nFAILED");
	exit(1);
}

struct image* bmp_read(const char *fname){
	FILE* file = fopen(fname, "rb");	
	if(file){
		struct image* image = image_create();
		enum read_status answer = from_bmp(file, image);
		fclose(file);
		if(answer != READ_OK){
			image_free(image);
			print_error(answer);
			return NULL;
		}
		return image;	
	}
	print_error(FILE_NOT_EXIST);
	return NULL;
}

void bmp_write(const char *fname, struct image* image){
	FILE* file = fopen(fname, "wb");
	if(file){
		enum write_status answer = to_bmp(file, (struct image const*)(image));
		fclose(file);
		if(answer != WRITE_OK){
			printf("Write error");
			image_free(image);
			exit(1);
		}	
	}
	else{
		printf("Write error");
		image_free(image);
		exit(1);
	}
}
