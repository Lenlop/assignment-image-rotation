#include "image_rotate.h"


size_t calculate_coordinate(size_t i, size_t j, size_t width){
	return j+width*i;
} 

struct image* rotate( struct image const source ){
	struct image* new_image = image_create();
	size_t height = source.height;
	size_t width = source.width;
	image_initialize(source.height, source.width, new_image);
	for(size_t i = 0; i<height; i++){
		for(size_t j = 0; j<width; j++){
			size_t index_rotated = calculate_coordinate(j, (height - i - 1), height);
			size_t index = calculate_coordinate(i, j, width);
			(new_image->data)[index_rotated] = source.data[index];
		}
	} 
	return new_image;
}
