#include "image.h"
 
struct image* image_create(void){
	struct image* image = malloc(sizeof(struct image));
	return image;
}

void image_initialize(uint64_t width, uint64_t height, struct image* image){
	image->width = width;
	image->height = height;
	image->data = malloc(sizeof(struct pixel)*(height)*(width));
}

void image_free(struct image* image){
	free(image->data);
	free(image);
} 
