#ifndef IMAGE_ROTATE_HEADER_FILE
#define IMAGE_ROTATE_HEADER_FILE

#include <stdio.h>
#include <malloc.h>
#include "image.h"

struct image* rotate( struct image const source );

#endif
