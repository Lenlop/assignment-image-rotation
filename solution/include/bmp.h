#ifndef BMP_HEADER_FILE
#define BMP_HEADER_FILE

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <malloc.h>
#include <stddef.h>
#include <sys/types.h>
#include "image.h"


enum read_status  {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_INVALID_RESERVED,
	FILE_NOT_EXIST
  };

enum  write_status  {
	WRITE_OK = 0,
	WRITE_ERROR
};

enum read_status from_bmp( FILE* in, struct image* img );
enum write_status to_bmp( FILE* out, struct image const* img );

struct image* bmp_read(const char *fname);
void bmp_write(const char *fname, struct image* image);

#endif
