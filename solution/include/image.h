#ifndef IMAGE_HEADER_FILE
#define IMAGE_HEADER_FILE

#include <stdlib.h>
#include<stdint.h>
#include <malloc.h>
#include<stdio.h>

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image {
  uint64_t width, height;
  struct pixel* data;
};


struct image* image_create(void);
void image_initialize(uint64_t width, uint64_t height, struct image* image);
void image_free(struct image* image);

#endif
